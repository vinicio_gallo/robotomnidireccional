﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Runtime.InteropServices;

public class Movimiento : MonoBehaviour
{
    const string dllPath = "smClient64.dll";
    //declarar las funciones de la dll
    [DllImport(dllPath)] // For 64 Bits System
    static extern int openMemory(String name, int type);


    [DllImport(dllPath)]
    static extern float getFloat(String memName, int position);


    private string memoriaMovimiento = "movimiento";


    //importar la dll de la memoria

    public Vector3 posInicial = new Vector3(0, 0, 0);
    public float velocidad = 0.5f;

    // Use this for initialization
    void Start()
    {
        transform.position = posInicial;
        openMemory(memoriaMovimiento, 2);
    }

    // Update is called once per frame
    void Update()
    {
            float movimientoHorizontal = getFloat(memoriaMovimiento, 0);
            float movimientoVertical = getFloat(memoriaMovimiento, 1);
        Vector3 temp = transform.position;
            temp.x = movimientoHorizontal;
            temp.z = movimientoVertical;
            transform.position = temp;   
    }
}

