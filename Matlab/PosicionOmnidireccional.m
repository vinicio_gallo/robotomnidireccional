clc
clear all
close all
%cargamos la memoria
loadlibrary('smClient64.dll','./smClient.h')

%abrimos la memoria para envio de movimiento
calllib('smClient64','openMemory','movimiento',2)
% Time
ts=0.1;
t=0.1:ts:60;

a=0.15;
b=0.10;
R=0.02;

T=(1/R)*[1 -1 -(a+b);...
         1  1  (a+b);...
         1  1  -(a+b);...
         1 -1  (a+b)];

%Condiciones iniciales
x(1)=0;
y(1)=0;
phi(1)=0;
   
%Velocidades
 uf(1)  = 0;
 ul(1)  = 0;
  % Trayectoria de un corazon
%     xd = (12*sin(0.1*t)-4*sin(3*0.1*t))/4; 
%     yd= (13*cos(0.1*t)-5*cos(2*0.1*t)-2*cos(3*0.1*t)-cos(4*0.1*t))/4; 
%   Trayectoria de un Seno en 'X'
    xd = 0.2*t;                         
    yd = sin(.3*t);  
  
   xdp = 0.2*ones(1,length(t)); 
   ydp = .3*cos(.3*t); 
% 

%     xdp= diff([0 xd]);  %Posici�n x1
%     ydp= diff([0 yd]);   %Posici�n y1   

   
xr(1)=x(1)+cos(phi(1));
yr(1)=y(1)+sin(phi(1));
    
for k=1:length(t)
   
    %Control
    xre(k)=xd(k)-xr(k);
    yre(k)=yd(k)-yr(k);
    e=[xre(k) yre(k)]';
    
    %Jacobiano
    J=[cos(phi(k)) -sin(phi(k));...
       sin(phi(k)) cos(phi(k))];
   
    %Ganancias
    K=[1 0;0 1];
   
    %Ley de control
      hdp=[xdp(k);ydp(k)];  
      v = inv(J)*(hdp+K*tanh(0.99*e));

    
    ul(k)=v(1); %%velocidad lineal lateral
    uf(k)=v(2); %%velocidad lineal frontal
    
    %Aplicar las acciones de control al robot
    xrp(k)=ul(k)*cos(phi(k))-uf(k)*sin(phi(k));
    yrp(k)=ul(k)*sin(phi(k))+uf(k)*cos(phi(k));
    
    w(k)=0;
    Wd=T*([ul(k);uf(k);w(k)]);
    %%Velocidad angular de los motores
    Wd1(k)=Wd(1);
    Wd2(k)=Wd(2);
    Wd3(k)=Wd(3);
    Wd4(k)=Wd(4);

    
    %%Encuentro las posiciones
    xr(k+1)=xr(k)+ts*xrp(k);
    yr(k+1)=yr(k)+ts*yrp(k);
    phi(k+1)=phi(k)+ts*w(k);
 
    x(k+1)=xr(k+1)-a*cos(phi(k+1));
    y(k+1)=yr(k+1)-a*sin(phi(k+1));
         
end

pasos=10;  fig=figure;
set(fig,'position',[10 60 980 600]);
axis square; cameratoolbar
axis([-4 12 -5 5 0 1]);
grid on
camlight right % scene light

Dimension_Omni;

M1=Plot_Omni(x(1),y(1),phi(1));hold on
M2=plot(xr(1),yr(1),'b','LineWidth',2);
xlabel('X'), ylabel('Y')

for i=1:pasos:length(t)
    
    delete (M1)
    delete (M2)
    M1=Plot_Omni(xr(i),yr(i),phi(i)); hold on
    M2=plot(xr(1:i),y(1:i),'b','LineWidth',2);
    yr(i)
%     %escribimos 
    calllib('smClient64','setFloat','movimiento',0,xr(i));
    calllib('smClient64','setFloat','movimiento',1,yr(i));

   pause(ts)
end

% Graficas
figure ('Name','Velocidad angular de motores')
subplot(411)
plot(t,Wd1,'linewidth',2), grid on
legend('Velocidad Wd1')
xlabel('Tiempo'), ylabel('Velocidad [rad/s]')
subplot(412)
plot(t,Wd2,'linewidth',2), grid on
legend('Velocidad Wd2')
xlabel('Tiempo'), ylabel('Velocidad [rad/s]')
subplot(413)
plot(t,Wd3,'linewidth',2), grid on
legend('Velocidad Wd3')
xlabel('Tiempo'), ylabel('Velocidad [rad/s]')
subplot(414)
plot(t,Wd4,'linewidth',2), grid on
legend('Velocidad Wd4')
xlabel('Tiempo'), ylabel('Velocidad [rad/s]')