function Mobile_Graph = Plot_Omni(dx,dy,angz)
global  Omni;

% Matriz de rotaci�n z

Rz=[ cos(angz) -sin(angz) 0; sin(angz) cos(angz) 0; 0 0 1];


robotPatch = Rz*Omni.platformVertices;
robotPatch(1,:)=robotPatch(1,:)+dx;
robotPatch(2,:)=robotPatch(2,:)+dy;

Mobile_Graph(1) = patch('Faces',Omni.platformFaces,'Vertices',robotPatch','FaceColor',[0 0.45 0.75],'EdgeColor','none');

robotPatch = Rz*Omni.platformfVertices;
robotPatch(1,:)=robotPatch(1,:)+dx;
robotPatch(2,:)=robotPatch(2,:)+dy;

Mobile_Graph(2) = patch('Faces',Omni.platformfFaces,'Vertices',robotPatch','FaceColor','green','EdgeColor','none');

robotPatch = Rz*Omni.wheelVertices;
robotPatch(1,:)=robotPatch(1,:)+dx;
robotPatch(2,:)=robotPatch(2,:)+dy;


Mobile_Graph(3) = patch('Faces',Omni.wheelFaces,'Vertices',robotPatch','FaceColor',[0 0 0],'EdgeColor','none');

robotPatch = Rz*Omni.wheelbVertices;
robotPatch(1,:)=robotPatch(1,:)+dx;
robotPatch(2,:)=robotPatch(2,:)+dy;

Mobile_Graph(4) = patch('Faces',Omni.wheelbFaces,'Vertices',robotPatch','FaceColor',[0.8 0.8 0.8],'EdgeColor','none');




