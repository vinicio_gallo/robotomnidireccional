function DimensionOmni(scale)

% ROBOT_DIMENSION(scale) loads robot physical parameters.
%   scale = scale to be used to plot the robot. Default values are in
%   meters.
load('OmniDimensions.mat')
if nargin < 1
    scale = 1;
end 

global Omni


% 1 m de largo.
% 0.5 m de ancho
Omni.platformVertices=PlatformOmni.vertices';
Omni.platformFaces=PlatformOmni.faces;

Omni.platformfVertices=PlatformFront.vertices';
Omni.platformfFaces=PlatformFront.faces;

%0.15 m radio
Omni.wheelVertices=wheelOmni.vertices';
Omni.wheelFaces=wheelOmni.faces;

Omni.wheelbVertices=wheelOmnib.vertices';
Omni.wheelbFaces=wheelOmnib.faces;

