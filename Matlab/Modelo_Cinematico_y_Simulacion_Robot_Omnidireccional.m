clc
clear all
close all

% Time
ts=0.1;
t=0:ts:20;

uf=0.1*ones(1,length(t));
ul=0.1*ones(1,length(t));
w=0.0*ones(1,length(t));

a=0.15;
b=0.10;
R=0.02;

T=(1/R)*[1 -1 -(a+b);...
         1  1  (a+b);...
         1  1  -(a+b);...
         1 -1  (a+b)];


xr(1)=0;
yr(1)=0;
phi(1)=0;


for k=1:length(t)
   
    xrp(k)=uf(k)*cos(phi(k))-ul(k)*sin(phi(k));
    yrp(k)=uf(k)*sin(phi(k))+ul(k)*cos(phi(k));
    
    
    Wd=T*([uf(k);ul(k);w(k)]);
    
    Wd1(k)=Wd(1);
    Wd2(k)=Wd(2);
    Wd3(k)=Wd(3);
    Wd4(k)=Wd(4);

    xr(k+1)=xr(k)+ts*xrp(k);
    yr(k+1)=yr(k)+ts*yrp(k);
    phi(k+1)=phi(k)+ts*w(k);
    
end

pasos=10;  fig=figure;
set(fig,'position',[10 60 980 600]);
axis square; cameratoolbar
grid on;
axis([-3 3 -3 3 0 1]); grid on
camlight right % scene light

Dimension_Omni;

M1=Plot_Omni(xr(1),yr(1),phi(1));hold on
M2 = plot(xr(1),yr(1),'','linewidth',2);

for i=1:pasos:length(t)
    
    delete (M1)
    delete (M2)
    M1=Plot_Omni(xr(i),yr(i),phi(i)); hold on
    M2=plot(xr(1:i),yr(1:i),'b','LineWidth',2);
    
   pause(ts)
end

% Graficas
figure('Name','Velocidad angular de motores')
subplot(411)
plot(t,Wd1,'linewidth',2), grid on
legend('Velocidad Wd1')
xlabel('Tiempo'), ylabel('Velocidad [rad/s]')
subplot(412)
plot(t,Wd2,'linewidth',2), grid on
legend('Velocidad Wd2')
xlabel('Tiempo'), ylabel('Velocidad [rad/s]')
subplot(413)
plot(t,Wd3,'linewidth',2), grid on
legend('Velocidad Wd3')
xlabel('Tiempo'), ylabel('Velocidad [rad/s]')
subplot(414)
plot(t,Wd4,'linewidth',2), grid on
legend('Velocidad Wd4')
xlabel('Tiempo'), ylabel('Velocidad [rad/s]')
